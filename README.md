# Docker in Development

This repository contains reference material for working with Docker while developing an application to reduce the headache of deploying.

## Core Concepts

These concepts will build the foundation of what Docker is so that you can better understand why we use it.

When you run an application on your personal machine from the command line (something like `python manage.py runserver` or `npm run start`), you are running that application in your system's _environment_. Your environment is defined by many things: a set of source code dependencies (e.g. node modules or python packages), a set of environment variables, and a user account.

Your user account is used by your operating system to determine which files you can read and write to and which files you can execute (among many other things). So once you get your code working "on your machine", you haven't actually determined if it will work when it is **deployed**. The deployment environment looks and acts differently. It will almost always be a server running some distribution of linux and there's a good chance that the server will execute your code with less privileges than you did when you were developing. This brings us to Containers.

### Containers / Images

Containers are (putting it simply) like virtual machines with less overhead and no graphical interface. You can run a container or a dozen containers even on your personal machine and they will behave more or less as if you had put a bunch of other computers on your desk and just networked them together. You can't open a web browser or anything else dependent on a graphical interface, but you can "exec" (more on that later) in to enter commands if you need to.

Containers are run by choosing an image and specifying a command. An image is the blueprint that describes things like the base operating system of the container, the installed dependencies, the default environment variables, the default command, and much more. Most containers have a default command specified.

### Networking & DNS

When you go to a web browser and type in <a href="visimo.ai">visimo.ai</a>, you are able to get there because there is a server somewhere that hosts the static files for that site, that server is listening on a specific port (or ports) on an IP address, and that IP address has a DNS record associated with it that says "when someone wants visimo.ai, send them to us".

That's all well and good, but why do we care? Well when you run a server on your local machine, you can access it by saying "localhost" or "127.0.0.1" and then a port number. This is doing the same thing as the visimo.ai example, but the DNS record is kept on your machine and the IP is an address on your local network, not the internet. When we run containers on our machines, they have their own addresses (and sometimes domains) on your network. The easiest way to access a container is by mapping that container to some port(s) when you launch it. That way when you go to `localhost:<whatever that port was>` you can access whatever the running application in the container is. Granted, this will only work if the application running in the container is listening for traffic (like a webserver does).

That's great for accessing one container, but what if we need multiple containers and we need them to talk to each other? Enter docker compose! Compose is an extra layer on top of docker that helps with the networking (because it can be a serious pain). Let's look at an example:

```yaml
# file: docker-compose.yaml
version: '3'

services:
  psql:
    # ...
  django:
    # ...
  react:
    # ...
```

Okay, the above is a simple (and truncated) docker compose file. The keys under `services` are the containers you want to run. For each container, you'll need to specify an image to use or a Dockerfile to build from, but this is more or less the gist. Now when you run this docker compose file it will start all of those containers and network them together so that each container can access each other by using the service names as DNS names. So if the django server needs to access the postgres server (psql), it can do so with `http://psql:<some_port>/`

### "Exec'ing"

We mentioned "exec'ing" before, but what does that mean? Well when you're running a server on your local machine, you often want to run some additional command for testing/development purposes. Something like `python manage.py makemigrations` if you're working with django. Well when the container starts running, you're no longer able to just access it willy-nilly, you now have to go through the docker interface to connect to it. If you are familiar with `ssh`, this is more or less the same process.

You can run the command `docker exec <some_running_container_name> <some_command>` to run that command inside of that container. If you want to actually have a terminal inside of the container that you can run multiple commands in and look at files, you'll need to specify the interactive flags and choose a shell as your command. Bash is a common shell that is available in many containers so to access bash we would write `docker exec -it <some_running_container_name> /bin/bash` where `-it` says interactive and `/bin/bash` is the path to the shell.

NOTE: Windows sucks a little bit and when you run the above command to exec in with bash, it will complain something like "command not recognized". That's because windows handles terminal input in a special way when you specify file paths (like `/bin/bash`). To get around this on windows, whenever you're specifying a command use `//` instead of `/` for the start of the path. In this example the command becomes `docker exec -it <some_running_container_name> //bin/bash`.

## Cheatsheet

Below is a list of commands you'll probably need a lot and what they do. Note, for any of the commands repeated with different flags, you can (usually) mix and match these commands.

| Command                                              | Description                                                                                                                                                                      |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `docker images`                                      | List the images currently saved on your machine                                                                                                                                  |
| `docker rmi <image_name>`                            | Delete an image from your machine                                                                                                                                                |
| `docker ps`                                          | List all running containers on your machine                                                                                                                                      |
| `docker ps -a`                                       | List all containers (not just the running ones) on your machine                                                                                                                  |
| `docker run <image_name>`                            | Turn an image into a running container.                                                                                                                                          |
| `docker run --rm <image_name>`                       | Same as `docker run`, but deletes the container when it stops.                                                                                                                   |
| `docker run --name <name_of_container> <image_name>` | Same as `docker run`, but names the container                                                                                                                                    |
| `docker exec <name_of_container> <command>`          | Run the command in the container.                                                                                                                                                |
| `docker exec -it <name_of_container> //bin/bash`     | Same as `docker exec`, but opens a terminal in the container. If the container complains that bash doesn't exist, try with `//bin/sh` instead.                                   |
| `docker-compose up`                                  | Start the containers (aka services) in your `docker-compose.yaml`, building or pulling them if you don't have the container already present on your machine.                     |
| `docker-compose up --build`                          | Same as `docker-compose up`, but always build the containers.                                                                                                                    |
| `docker-compose up -d`                               | Same as `docker-compose up`, but don't show the logs. The `d` stands for "detached" as in "detached from the output".                                                            |
| `docker-compose down`                                | Stop the containers from your docker-compose and delete them.                                                                                                                    |
| `docker-compose stop`                                | Stop the containers from your docker-compose but don't delete them. Note, this is what happens by default if you use `docker-compose up` and then ctrl+c or close your terminal. |
| `docker-compose logs`                                | Show the logs from a running docker-compose. Typically you would use this after you used `docker-compose up -d`.                                                                 |
| `docker-compose logs -f`                             | Same as `docker-compose logs`, but stream the logs instead of exiting.                                                                                                           |
